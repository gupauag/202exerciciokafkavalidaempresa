package com.br.Service;

import com.br.Model.Cliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class ClienteProducer {

    @Autowired
    private KafkaTemplate<String, Cliente> producer;

    public void enviarAoKafka(Cliente cliente) {
        System.out.println("CNPJ: "+cliente.getCnpj());
        producer.send("spec3-gustavo-aguiar-2", cliente);
    }

}
