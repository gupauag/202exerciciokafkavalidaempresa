package com.br.Service;

import com.br.Model.Cliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClienteService {

    @Autowired
    private ClienteProducer clienteProducer;

    public void criaCliente(Cliente cliente){
        //só vai mandar para o kafka
        clienteProducer.enviarAoKafka(cliente);
    }

}
