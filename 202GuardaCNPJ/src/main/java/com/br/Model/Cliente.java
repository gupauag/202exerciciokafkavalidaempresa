package com.br.Model;

public class Cliente {
    private long cnpj;

    public Cliente() {
    }

    public long getCnpj() {
        return cnpj;
    }

    public void setCnpj(long cnpj) {
        this.cnpj = cnpj;
    }
}
