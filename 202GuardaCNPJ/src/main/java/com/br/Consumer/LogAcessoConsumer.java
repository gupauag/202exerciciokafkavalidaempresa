package com.br.Consumer;

import com.br.Model.Cliente;
import com.opencsv.CSVWriter;
import com.opencsv.CSVWriterBuilder;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import javax.sound.midi.Patch;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class LogAcessoConsumer {

    @KafkaListener(topics = "spec3-gustavo-aguiar-3", groupId = "GustavoAraujo-1")

    public void receber(@Payload Cliente cliente) throws Exception {
        System.out.println("CNPJ: " + cliente.getCnpj());
        montaArquivoCSV(cliente);
    }

    public void montaArquivoCSV(Cliente cliente){
//        File file = new File("/home/a2w/workspace/202exerciciokafkavalidaempresa/LogAcesso.csv");
//        Writer writer = new FileWriter(file, true);
//
//        StatefulBeanToCsv<Cliente> beanToCsv = new StatefulBeanToCsvBuilder(writer).build();
//        beanToCsv.write(cliente);
//        writer.flush();
//        writer.close();

        File file = new File("/home/a2w/workspace/202exerciciokafkavalidaempresa/LogAcesso.csv");
        FileWriter fileWriter = null;
        PrintWriter wr = null;
        try {
            // Cria um writer para um arquivo com modo incremental ligado.
            fileWriter = new FileWriter(file, true);
            // cria um writer mais amigável para escrita.
            wr = new PrintWriter(fileWriter);
            //Cria o cabeçalho
            if(file.length()==0){
                wr.write("CNPJ, DataAtualização\n");
            }
            // Escreve o texto passado como parâmetro e quebra a linha.
            wr.write(""+cliente.getCnpj());
            wr.write(","+new Date()+"\n");
        } catch (IOException ex) {
        } finally {
            // liberando recursos
            try {
                wr.close();
                fileWriter.close();
            } catch (IOException ex) {
                System.err.println("fileWriter.close() lançou uma exceção: " + ex.getMessage());
            }
        }

    }



}
