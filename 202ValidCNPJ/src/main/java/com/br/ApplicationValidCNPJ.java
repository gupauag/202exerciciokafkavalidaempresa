package com.br;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class ApplicationValidCNPJ {

	public static void main(String[] args) {
		SpringApplication.run(ApplicationValidCNPJ.class, args);
	}

}
