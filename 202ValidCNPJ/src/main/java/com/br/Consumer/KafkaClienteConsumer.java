package com.br.Consumer;

import com.br.Model.Cliente;
import com.br.Service.ValidaCNPJService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class KafkaClienteConsumer {

    @Autowired
    private ValidaCNPJService validaCNPJService;

    @KafkaListener(topics = "spec3-gustavo-aguiar-2", groupId = "GustavoAraujo-1")
    public void receber(@Payload Cliente cliente) throws Exception {

        validaCNPJService.validaEnviaCNPJ(cliente);

    }



}
