package com.br.Client.DTO;

import java.math.BigDecimal;

public class ReceitaResponse {

    public BigDecimal capital_social;

    public ReceitaResponse(){}

    public BigDecimal getCapital_social() {
        return capital_social;
    }

    public void setCapital_social(BigDecimal capital_social) {
        this.capital_social = capital_social;
    }
}
