package com.br.Client;

import com.br.Client.DTO.ReceitaResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;

@FeignClient(name = "receitaws", url = "https://www.receitaws.com.br/v1/cnpj")
public interface ReceitaClient {

    @GetMapping("/{cnpj}")
    Optional<ReceitaResponse> consultaCNPJ(@PathVariable String cnpj);

}
