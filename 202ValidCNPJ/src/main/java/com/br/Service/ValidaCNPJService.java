package com.br.Service;

import com.br.Client.DTO.ReceitaResponse;
import com.br.Client.ReceitaClient;
import com.br.Model.Cliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ValidaCNPJService {

    @Autowired
    private ReceitaClient receitaClient;

    @Autowired
    private ClienteProducer clienteProducer;

    public void validaEnviaCNPJ(Cliente cliente) {
        try {
            Optional<ReceitaResponse> clienteOptional = receitaClient.consultaCNPJ(cliente.getCnpj());

            if (clienteOptional.isPresent()) {
                if (clienteOptional.get().getCapital_social().doubleValue() > 1000000.00) {
                    clienteProducer.enviarAoKafka(cliente);
                }
                System.out.println("Empresa "+ cliente.getCnpj() +
                        "com captal: "+clienteOptional.get().getCapital_social().doubleValue());
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }


    }

}
