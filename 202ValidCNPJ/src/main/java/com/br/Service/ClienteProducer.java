package com.br.Service;

import com.br.Model.Cliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class ClienteProducer {

    @Autowired
    private KafkaTemplate<String, Cliente> producer;

    public void enviarAoKafka(Cliente cliente) {
        System.out.println("Enviando cliente "+ cliente.getCnpj()+ " para o 3º serviço de gravação CVS");
        producer.send("spec3-gustavo-aguiar-3", cliente);
    }

}
